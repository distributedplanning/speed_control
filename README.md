# README #

This package is a low level controller for differential drive robots such as the irobotcreate.
It receives as inputs a list of targets in space and time, and tries to reach them at the desired time.